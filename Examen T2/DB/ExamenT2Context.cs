﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Examen_T2.DB.Mapping;
using Examen_T2.Models;
using Microsoft.EntityFrameworkCore;



namespace Examen_T2.DB
{
    public class ExamenT2Context:DbContext
    {
        public DbSet<Usuario> Usuarios { get; set; }
        public DbSet<Pokemon> Pokemons { get; set; }
        public DbSet<Tipo> Tipos { get; set; }
        public DbSet<UsuarioPokemon> UsuarioPokemons { get; set; }

        public ExamenT2Context(DbContextOptions<ExamenT2Context> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.ApplyConfiguration(new UsuarioMap());
            modelBuilder.ApplyConfiguration(new TipoMap());
            modelBuilder.ApplyConfiguration(new PokemonMap());
            modelBuilder.ApplyConfiguration(new UsuarioPokemonMap());
        }



    }
}
