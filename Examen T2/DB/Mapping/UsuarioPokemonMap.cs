﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Examen_T2.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace Examen_T2.DB.Mapping
{
    public class UsuarioPokemonMap : IEntityTypeConfiguration<UsuarioPokemon>
    {
        public void Configure(EntityTypeBuilder<UsuarioPokemon> builder)
        {
            builder.ToTable("UsuarioPokemon");
            builder.HasKey(o => o.Id);
            builder.HasOne(o => o.Pokemon).WithMany().HasForeignKey(o => o.PokemonId);
        }

    }
}
