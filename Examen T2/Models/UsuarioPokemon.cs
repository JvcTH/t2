﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Examen_T2.Models
{
    public class UsuarioPokemon
    {
        public int Id { get; set; }
        public DateTime FechaCaptura { get; set; }
        public int PokemonId { get; set; }
        public int UsuarioId { get; set; }
        public Pokemon Pokemon { get; set; }
    }
}
