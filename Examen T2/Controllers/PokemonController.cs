﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Examen_T2.DB;
using Examen_T2.Models;
using System.IO;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;

namespace Examen_T2.Controllers
{
    public class PokemonController : Controller
    {
        private ExamenT2Context context;
        private IWebHostEnvironment hosting;
        public PokemonController(ExamenT2Context context, IWebHostEnvironment hosting)
        {
            this.context = context;
            this.hosting = hosting;
        }
        public IActionResult Index()
        {
            return View(context.Pokemons.ToList());
        }

        public IActionResult Create()
        {
            ViewBag.Tipos = context.Tipos.ToList();
            return View();
        }

        public IActionResult Save(Pokemon pokemon, IFormFile file)
        {
            if (ModelState.IsValid)
            {
                pokemon.Imagen = SaveFile(file);
                context.Pokemons.Add(pokemon);
                context.SaveChanges();

                return RedirectToAction("Index");
            }

            ViewBag.Tipos = context.Tipos.ToList();
            return View(pokemon);

        }
        public IActionResult CatchPokemon(int pokemonId)
        {
            var user = GetLoggedUser();
            context.UsuarioPokemons.Add(new UsuarioPokemon { PokemonId = pokemonId, FechaCaptura = DateTime.Now, UsuarioId = user.Id });
            context.SaveChanges();
            return RedirectToAction("Capturados");

        }
        public IActionResult Capturados()
        {
            var user = GetLoggedUser();
            var capturados=context.UsuarioPokemons.Where(o=> o.UsuarioId==user.Id).Include(o=>o.Pokemon).ToList();

            return View(capturados);
        }

        public IActionResult LiberarPokemon(int pokemonId)
        {
            var pokemon = context.UsuarioPokemons.Where(o=> o.Id==pokemonId).First();
            context.UsuarioPokemons.Remove(pokemon);
            context.SaveChanges();
            return RedirectToAction("Capturados");

        }

        private Usuario GetLoggedUser()
        {
            var claim = HttpContext.User.Claims.First();
            string username = claim.Value;
            var user = context.Usuarios.First(o => o.Username == username);
            return user;
        }


        private string SaveFile(IFormFile file)
        {
            string relativePath = "";

            if (file.Length > 0 && (file.ContentType == "image/png" || file.ContentType == "image/jpg"))
            {
                relativePath = Path.Combine("files", file.FileName);
                var filePath = Path.Combine(hosting.WebRootPath, relativePath);
                var stream = new FileStream(filePath, FileMode.Create);
                file.CopyTo(stream);
                stream.Close();
            }

            return "/" + relativePath.Replace('\\', '/');
        }

    }
}
